
import CommentDetailContainer from './CommentDetailContainer';
import Header from './sub-comp/Header';
import Content from './sub-comp/Content';


type CommentDetailComponent = typeof CommentDetailContainer & {
  Header: typeof Header;
  Content: typeof Content;
};

const CommentDetail = CommentDetailContainer as CommentDetailComponent;

CommentDetail.Header = Header;
CommentDetail.Content = Content;

export default CommentDetail;
