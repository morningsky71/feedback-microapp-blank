import React, { ContextType } from 'react';
import { observer } from 'mobx-react';
import moment from 'moment';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { Avatar, Box, Card, Link, Typography } from '@nara.platform/react-ui';
import { AccessTime } from '@material-ui/icons';

import { Comment } from '~/comp/api';
import CommentDetailContext from '../../context/CommentDetailContext';


interface Props {
  //
  anonymous?: boolean;
  renderAction?: (comment: Comment) => React.ReactNode;
}

@autobind
@observer
class HeaderView extends ReactComponent<Props> {
  //
  static defaultProps = {
    anonymous: false,
    renderAction: () => null,
  };

  static contextType = CommentDetailContext;

  context!: ContextType<typeof CommentDetailContext>;

  render() {
    //
    const { anonymous, renderAction } = this.propsWithDefault;
    const { comment } = this.context.commentDetail;

    return (
      <Card.Header
        disableTypography
        avatar={<Avatar alt={comment.writerName} src={!anonymous ? comment.writerId : ''} />}
        title={(
          <Link color="textPrimary" variant="h6">
            {!anonymous ? comment.writerName : 'anonymous'}
          </Link>
        )}
        subheader={(
          <Box display="flex" alignItems="center" color="text.secondary">
            <Box display="flex" pr={1}>
              <AccessTime fontSize="small" />
            </Box>
            <Typography variant="body2">
              {moment(comment.time).fromNow()}
            </Typography>
          </Box>
        )}
        action={renderAction(comment)}
      />
    );
  }
}

export default HeaderView;
