
import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { observer } from 'mobx-react';
import { Card } from '@nara.platform/react-ui';

import { Comment } from '~/comp/api';
import CommentDetailContext, { CommentDetailContextModel } from './context/CommentDetailContext';


interface Props {
  //
  comment: Comment;
  children: React.ReactNode;
}


@autobind
@observer
class CommentDetailContainer extends ReactComponent<Props> {
  //
  getContext(): CommentDetailContextModel {
    //
    const { comment } = this.props;

    return {
      commentDetail: {
        comment,
      },
    };
  }

  render() {
    //
    const { children } = this.props;

    return (
      <CommentDetailContext.Provider value={this.getContext()}>
        <Card>
          {children}
        </Card>
      </CommentDetailContext.Provider>
    );
  }
}

export default CommentDetailContainer;
