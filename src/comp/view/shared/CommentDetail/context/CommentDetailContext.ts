
import React from 'react';
import { Comment } from '~/comp/api';


export type CommentDetailContextModel = {
  commentDetail: {
    comment: Comment;
  };
};

const CommentDetailContext = React.createContext<CommentDetailContextModel>({
  commentDetail: {
    comment: {} as Comment,
  },
});

export default CommentDetailContext;
