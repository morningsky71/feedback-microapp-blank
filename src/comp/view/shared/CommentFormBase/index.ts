
import CommentFormBaseView from './CommentFormBaseView';
import Comment from './sub-comp/Comment';
import AttachedImage from './sub-comp/AttachedImage';
import Actions from './sub-comp/Actions';


type CommentFormBaseComponent = typeof CommentFormBaseView & {
  Comment: typeof Comment;
  AttachedImage: typeof AttachedImage;
  Actions: typeof Actions;
};

const CommentFormBase = CommentFormBaseView as CommentFormBaseComponent;

CommentFormBase.Comment = Comment;
CommentFormBase.AttachedImage = AttachedImage;
CommentFormBase.Actions = Actions;

export default CommentFormBase;
