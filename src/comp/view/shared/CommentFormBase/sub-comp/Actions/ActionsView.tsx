import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { Box, Button, Typography } from '@nara.platform/react-ui';
import { Send } from '@material-ui/icons';
import { WithStyles, withStyles } from './style';
import ImageUploadButton from '../../../ImageUploadButton';


interface Props extends WithStyles {
  message: string;
  maxLength: number;
  hideCamera: boolean;
  onChangeImage: (reader: FileReader) => void;
  onSubmit: (event: React.MouseEvent) => void;
}


@autobind
@observer
class ActionsView extends ReactComponent<Props> {
  //
  render() {
    const { classes, message, maxLength, hideCamera, onChangeImage, onSubmit } = this.props;

    return (
      <Box className={classes.actions} mt={1} justifyContent="space-between" alignItems="center">
        <Box>
          <Typography display="inline">{message.length}</Typography>
          <Typography display="inline">/</Typography>
          <Typography display="inline" color="textSecondary">{maxLength}</Typography>
        </Box>
        {!hideCamera && (
          <ImageUploadButton onCompleteLoad={onChangeImage} />
        )}
        <Button color="secondary" disabled={message.length > maxLength} onClick={onSubmit}><Send /> 등록</Button>
      </Box>
    );
  }
}

export default withStyles(ActionsView);
