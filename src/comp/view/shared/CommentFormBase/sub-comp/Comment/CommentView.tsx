
import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { TextField } from '@nara.platform/react-ui';


interface Props {
  message: string;
  maxLength: number;
  rows: number;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}


@autobind
@observer
class CommentView extends ReactComponent<Props> {
  //
  render() {
    const { message, maxLength, rows, onChange } = this.props;

    return (
      <TextField
        placeholder="Leave a comment"
        fullWidth
        multiline={rows !== 1}
        rows={rows}
        rowsMax={rows}
        name="message"
        value={message || ''}
        error={message.length > maxLength}
        onChange={onChange}
      />
    );
  }
}

export default CommentView;
