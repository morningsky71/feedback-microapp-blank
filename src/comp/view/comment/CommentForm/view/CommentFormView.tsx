
import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { Box } from '@nara.platform/react-ui';

import { CommentFormBase } from '~/comp/view/shared';


interface Props {
  message: string;
  base64AttachedImage: string;
  maxLength: number;
  rows: number;
  hideCamera: boolean;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onChangeImage: (reader: FileReader) => void;
  onRemoveImage: (event: React.MouseEvent) => void;
  onSubmit: (event: React.MouseEvent) => void;
}


@autobind
@observer
class CommentFormView extends ReactComponent<Props> {
  //
  render() {
    const { message, base64AttachedImage, maxLength, rows, hideCamera, onChange, onChangeImage, onRemoveImage, onSubmit } = this.props;

    return (
      <CommentFormBase>
        <Box p={2}>
          <CommentFormBase.Comment
            message={message}
            maxLength={maxLength}
            rows={rows}
            onChange={onChange}
          />
          <CommentFormBase.AttachedImage
            base64AttachedImage={base64AttachedImage}
            onRemoveImage={onRemoveImage}
          />
        </Box>

        <CommentFormBase.Actions
          message={message}
          maxLength={maxLength}
          hideCamera={hideCamera}
          onChangeImage={onChangeImage}
          onSubmit={onSubmit}
        />
      </CommentFormBase>
    );
  }
}

export default CommentFormView;
