import React from 'react';
import { observer } from 'mobx-react';
import { autobind, DramaException, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { Comment } from '../../../api';
import { CommentStateKeeper } from '../../../state';
import CommentFormView from './view/CommentFormView';


interface Props {
  sourceEntityId: string;
  sourceEntityName: string;
  commentId?: string;
  rows?: number;
  hideCamera?: boolean;
  onSuccess?: () => void;
  onFail?: () => void;
  userId: string;
  writerName: string;
}

interface InjectedProps {
  //
  commentStateKeeper: CommentStateKeeper;
}

@autobind
@observer
class CommentFormContainer extends ReactComponent<Props, {}, InjectedProps> {
  //
  static defaultProps = {
    commentId: '',
    config: {},
    rows: 5,
    hideCamera: true,
    onSuccess: () => {},
    onFail: () => {},
    userId: '',
    writerName: '',
  };

  componentDidMount() {
    //
    this.init();
  }

  componentDidUpdate(prevProps: Readonly<Props>) {
    //
    const { sourceEntityId: prevSourceEntityId } = prevProps;
    const { sourceEntityId } = this.props;

    if (prevSourceEntityId !== sourceEntityId) {
      this.init();
    }
  }

  async init() {
    //
    await this.initComment();
  }

  async initComment() {
    //
    const { commentId } = this.propsWithDefault;
    const { commentStateKeeper } = this.injected;

    if (commentId) {
      
      commentStateKeeper.findCommentById(commentId);

    } else {

      const { sourceEntityId, userId, writerName} = this.props;
      commentStateKeeper.initComment(userId, writerName, sourceEntityId);
    }
  }

  onChange(event: React.ChangeEvent<HTMLInputElement>) {

    //
    // TODO: comment의 property 수정
    const { commentStateKeeper } = this.injected;
    const { comment } = commentStateKeeper;

    commentStateKeeper.setCommentProp('message',event.target.value);

  }

  onChangeImage(reader: FileReader) {
    //
    const { commentStateKeeper } = this.injected;

    commentStateKeeper.setCommentProp('base64AttachedImage', reader.result);
  }

  onRemoveImage() {
    //
    const { commentStateKeeper } = this.injected;

    commentStateKeeper.setCommentProp('base64AttachedImage', '');
  }

  async onSubmit() {
    //
    const { onSuccess, onFail } = this.propsWithDefault;
    const { commentStateKeeper } = this.injected;
    const { comment } = commentStateKeeper;

    if (!comment) {
      throw new DramaException('CommentForm', 'Comment and comment feedback should not be null.');
    }


    // TODO: Comment 저장 후 결과에 따라 onSuccess/onFail 실행
    debugger;
    const commandResponse = await commentStateKeeper.save(comment);

    if (commandResponse.result) {
      onSuccess();
    }
    else {
      onFail();
    }    

    await this.initComment();
  }

  render() {
    const { rows, hideCamera } = this.propsWithDefault;
    const { commentStateKeeper } = this.injected;
    const { comment } = commentStateKeeper;

    if (!comment) {
      return null;
    }

    return (
      <CommentFormView
        message={comment.message}
        base64AttachedImage={comment.base64AttachedImage}
        maxLength={140}
        rows={rows}
        hideCamera={hideCamera}
        onChange={this.onChange}
        onChangeImage={this.onChangeImage}
        onRemoveImage={this.onRemoveImage}
        onSubmit={this.onSubmit}
      />
    );
  }
}

export default ServiceInjector.withContext(
  CommentStateKeeper
)(CommentFormContainer);
