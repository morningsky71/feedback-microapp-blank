import React from 'react';
import {
  autobind,
  Offset,
  OffsetElementList,
  ReactComponent,
  ServiceInjector,
  SortDirection,
} from '@nara.drama/prologue';
import { observer } from 'mobx-react';
import { CommentsStateKeeper, CommentStateKeeper } from '../../../state';
import { Comment } from '~/comp/api';
import CommentListContext, { CommentListContextModel } from './context/CommentListContext';



interface Props {
  //
  children: React.ReactNode;
  sourceEntityId: string;
  sourceEntityName: string;
  userId: string;
  limit?: number;
  showDeleted?: boolean;
  onInit?: (totalCount: number) => void;
  writerName: string;
}

interface InjectedProps {
  //
  commentsStateKeeper: CommentsStateKeeper;
  commentStateKeeper: CommentStateKeeper;
}

@autobind
@observer
class CommentListContainer extends ReactComponent<Props, {}, InjectedProps> {
  //
  static defaultProps = {
    limit: 15,
    showDeleted: false,
    onInit: () => {},
    writerName: '',
  };

  componentDidMount() {
    //
    this.init();
  }

  componentDidUpdate(prevProps: Readonly<Props>) {
    //
    const { sourceEntityId: prevSourceEntityId } = prevProps;
    const { sourceEntityId } = this.props;

    if (prevSourceEntityId !== sourceEntityId) {
      this.init();
    }
  }

  async init(sortDirection?: SortDirection) {
    //
    const { onInit } = this.propsWithDefault;
    const { sourceEntityId } = this.props;
    const commentOffsetElementList = await this.initComments(sourceEntityId, sortDirection);

    onInit(commentOffsetElementList.totalCount);
  }

  async initComments(commentFeedbackSourceEntityId: string, sortDirection?: SortDirection): Promise<OffsetElementList<Comment>> {
    //
    const { limit, showDeleted } = this.propsWithDefault;
    const { commentsStateKeeper } = this.injected;
    const { offset } = commentsStateKeeper;

    const targetLimit = offset.limit || limit;
    const targetOffset = new Offset(0, targetLimit);

    targetOffset.sortDirection = sortDirection || SortDirection.Descending;
    targetOffset.sortingField = 'time';

    let commentOffsetElementList;

    if (showDeleted) {
      commentOffsetElementList = await commentsStateKeeper.findCommentsByFeedbackId(commentFeedbackSourceEntityId, targetOffset);
    }
    else {
      commentOffsetElementList = await commentsStateKeeper.findNotDeletedCommentsByFeedbackId(commentFeedbackSourceEntityId, targetOffset);
    }

    return commentOffsetElementList;
  }

  getContext(): CommentListContextModel {
    //
    const { sourceEntityId, sourceEntityName, userId, writerName } = this.props;

    return {
      commentList: {
        userId,
        sourceEntityId,
        sourceEntityName,
        init: this.init,
        findMoreComments: this.findMoreComments,
        writerName,
      },
    };
  }

  findMoreComments() {
    //
    const { sourceEntityId } = this.props;
    const { commentsStateKeeper } = this.injected;
    const { offset } = commentsStateKeeper;

    const newOffset = { ...offset };

    newOffset.limit = offset.limit + offset.limit;

    commentsStateKeeper.findCommentsByFeedbackId(sourceEntityId, newOffset);
  }

  renderChildren() {
    //
    const { children } = this.props;
    let targetChildren = children;

    if (typeof children === 'function') {
      targetChildren = children({
        init: this.init,
      });
    }

    return targetChildren;
  }

  render() {
    //
    return (
      <CommentListContext.Provider value={this.getContext()}>
        {this.renderChildren()}
      </CommentListContext.Provider>
    );
  }
}

export default ServiceInjector.withContext(
  CommentsStateKeeper,
  CommentStateKeeper
)(CommentListContainer);
