
import React from 'react';
import { SortDirection } from '@nara.drama/prologue';


export type CommentListContextModel = {
  commentList: {
    userId: string;
    sourceEntityId: string;
    sourceEntityName: string;
    init: (sortDirection?: SortDirection) => void;
    findMoreComments: () => void;
    writerName: string;
  };
};

const CommentListContext = React.createContext<CommentListContextModel>({
  commentList: {
    userId: '',
    sourceEntityId: '',
    sourceEntityName: '',
    init: () => {},
    findMoreComments: () => {},
    writerName: '',
  },
});

export default CommentListContext;
