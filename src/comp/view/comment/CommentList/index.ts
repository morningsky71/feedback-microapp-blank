import { default as CommentListContextParams } from './model/CommentListContextParams';
import CommentListContainer from './CommentListContainer';
import Header from './sub-comp/CommentListHeader';
import Content from './sub-comp/CommentListContent';
import ActionButton from './sub-comp/CommentListActionButton';


type CommentListComponent = typeof CommentListContainer & {
  Header: typeof Header;
  Content: typeof Content;
  ActionButton: typeof ActionButton;
};

const CommentList = CommentListContainer as CommentListComponent;

CommentList.Header = Header;
CommentList.Content = Content;
CommentList.ActionButton = ActionButton;

export default CommentList;
export type {
  CommentListContextParams
};
