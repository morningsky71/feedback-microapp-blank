import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';

import { Comment } from '~/comp/api';
import CommentDetail from '../../../../../shared/CommentDetail';
import CommentForm from '../../../../CommentForm';


interface Props {
  //
  comment: Comment;
  sourceEntityId: string;
  sourceEntityName: string;
  onClickReplies: (comment: Comment) => void;
  onModify: (success: boolean) => void;
  userId: string;
  writerName: string;
}

@autobind
@observer
class CommentDetailContentView extends ReactComponent<Props> {
  //
  render() {
    //
    const { comment, sourceEntityId, sourceEntityName, onClickReplies, onModify, userId, writerName } = this.props;

    return comment.editing ? (
      <CommentForm
        commentId={comment.id}
        sourceEntityId={sourceEntityId}
        sourceEntityName={sourceEntityName}
        onSuccess={() => onModify(true)}
        onFail={() => onModify(false)}
        userId={userId}
        writerName={writerName}
      />
    ) : (
      <CommentDetail.Content
        onClickReplies={onClickReplies}
      />
    );
  }
}

export default CommentDetailContentView;
