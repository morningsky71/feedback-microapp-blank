import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { Box } from '@nara.platform/react-ui';

import { Comment } from '~/comp/api';
import { CommentAction } from '~/comp/view/shared';
import CommentDetail from '../../../../../shared/CommentDetail';
import CommentDetailContentView from './CommentDetailContentView';


interface Props {
  //
  userId: string;
  sourceEntityId: string;
  sourceEntityName: string;
  comment: Comment;
  anonymous: boolean;
  onClickEdit: (commentId: string) => void;
  onClickRemove: (commentId: string) => void;
  onClickReplies: (comment: Comment) => void;
  onModify: (success: boolean) => void;
  writerName: string;
}

@autobind
@observer
class CommentDetailView extends ReactComponent<Props> {
  //
  render() {
    //
    const {
      userId, sourceEntityId, sourceEntityName, comment, anonymous,
      onClickEdit, onClickRemove, onClickReplies, onModify,
      writerName,
    } = this.props;

    return (
      <Box mb={2}>
        <CommentDetail comment={comment}>
          <CommentDetail.Header
            anonymous={anonymous}
            renderAction={(targetComment: Comment) => comment.writerId === userId && !comment.deleted && (
              <CommentAction
                commentId={targetComment.id}
                onClickEdit={onClickEdit}
                onClickRemove={onClickRemove}
              />
            )}
          />
          <CommentDetailContentView
            comment={comment}
            sourceEntityId={sourceEntityId}
            sourceEntityName={sourceEntityName}
            onClickReplies={onClickReplies}
            onModify={onModify}
            userId={userId}
            writerName={writerName}
          />
        </CommentDetail>
      </Box>
    );
  }
}

export default CommentDetailView;
