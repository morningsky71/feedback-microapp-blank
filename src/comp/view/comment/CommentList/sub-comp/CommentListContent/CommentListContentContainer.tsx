import React, { ContextType } from 'react';
import { autobind, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { observer } from 'mobx-react';

import { Comment } from '~/comp/api';
import { CommentsStateKeeper, CommentStateKeeper } from '~/comp/state';
import CommentListContext from '../../context/CommentListContext';
import CommentDetailView from './view/CommentDetailView';
import { dialog } from '@nara.platform/react-ui';


interface Props {
  //
  onClickEdit?: (comment: Comment) => void;
  onSuccess?: () => void;
  onFail?: () => void;
}

interface InjectedProps {
  //
  commentsStateKeeper: CommentsStateKeeper;
  commentStateKeeper: CommentStateKeeper;
}

@autobind
@observer
class CommentListContentContainer extends ReactComponent<Props, {}, InjectedProps> {
  //
  static defaultProps = {
    onClickEdit: () => {},
    renderSub: undefined,
    onSuccess: () => {},
    onFail: () => {},
  };

  static contextType = CommentListContext;
  context!: ContextType<typeof CommentListContext>;

  onToggle(comment: Comment) {
    //
    const { commentsStateKeeper } = this.injected;
    commentsStateKeeper.setCommentsProp(comment.id, 'expanded', !comment.expanded);
  }

  onClickEdit(commentId: string) {
    //
    const { commentsStateKeeper } = this.injected;
    commentsStateKeeper.setCommentsProp(commentId, 'editing', true);
  }

  async onRemove(commentId: string) {

    
    //
    // TODO: 코멘트 삭제 후 결과에 따라 this.onComplete 호출
    const { commentStateKeeper } = this.injected;
    
    const confirmed = await dialog.confirm({ title: '삭제', message: '코멘트를 삭제 하시겠습니까?' });
    if (!confirmed) {
      return;
    }

    const response = await commentStateKeeper.remove(commentId);
  
    if (response.entityIds.length) {
      this.onComplete(true);
    }
    else {
      this.onComplete(false);
    }

  }

  onModify(success: boolean) {
    //
    const { commentList } = this.context;

    commentList.init();
    this.onComplete(success);
  }

  onComplete(success: boolean) {
    //
    const { commentList } = this.context;
    const { onSuccess, onFail } = this.propsWithDefault;

    if (success) {
      onSuccess();
    }
    else {
      onFail();
    }

    commentList.init();
  }

  render() {
    //
    const { commentList } = this.context;
    const { writerName } = commentList;

    const { commentsStateKeeper } = this.injected;
    const { comments } = commentsStateKeeper;

    return comments.map((comment, index) => (
      <CommentDetailView
        key={index}
        comment={comment}
        userId={commentList.userId}
        sourceEntityId={commentList.sourceEntityId}
        sourceEntityName={commentList.sourceEntityName}
        anonymous={false}
        onClickEdit={this.onClickEdit}
        onClickRemove={this.onRemove}
        onClickReplies={this.onToggle}
        onModify={this.onModify}
        writerName={writerName}
      />
    ));
  }
}

export default ServiceInjector.useContext(
  CommentsStateKeeper,
  CommentStateKeeper
)(CommentListContentContainer);
