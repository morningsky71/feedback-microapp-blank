import React, { ContextType } from 'react';
import { autobind, ReactComponent, ServiceInjector, SortDirection } from '@nara.drama/prologue';
import { observer } from 'mobx-react';
import { FormControl, Radio, SubActions } from '@nara.platform/react-ui';

import { CommentsStateKeeper } from '~/comp/state';
import CommentListContext from '../../context/CommentListContext';


interface InjectedProps {
  //
  commentsStateKeeper: CommentsStateKeeper;
}

@autobind
@observer
class CommentListHeaderContainer extends ReactComponent<{}, {}, InjectedProps> {
  //
  static contextType = CommentListContext;

  context!: ContextType<typeof CommentListContext>;

  onChange(event: React.ChangeEvent<HTMLInputElement>) {
    //
    const { commentList } = this.context;
    const sortDirection = event.target.value as SortDirection;

    commentList.init(sortDirection);
  }

  render() {
    const { commentsStateKeeper } = this.injected;
    const { offset } = commentsStateKeeper;

    return (
      <SubActions>
        <SubActions.Right>
          <FormControl.ControlLabel
            label="Newest"
            control={
              <Radio
                className="base"
                name="radioGroup"
                value={SortDirection.Descending}
                checked={offset.sortDirection !== SortDirection.Ascending}
                onChange={this.onChange}
              />
            }
          />
          <FormControl.ControlLabel
            label="Oldest"
            control={
              <Radio
                className="base"
                name="radioGroup"
                value={SortDirection.Ascending}
                checked={offset.sortDirection === SortDirection.Ascending}
                onChange={this.onChange}
              />
            }
          />
        </SubActions.Right>
      </SubActions>
    );
  }
}

export default ServiceInjector.useContext(
  CommentsStateKeeper
)(CommentListHeaderContainer);
