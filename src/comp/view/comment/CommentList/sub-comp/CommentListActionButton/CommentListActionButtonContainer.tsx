import React, { ContextType } from 'react';
import { autobind, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { observer } from 'mobx-react';
import { Box, Button } from '@nara.platform/react-ui';

import { CommentsStateKeeper } from '~/comp/state';
import CommentListContext from '../../context/CommentListContext';


interface Props {
  //
  onClick?: (event: React.MouseEvent) => void;
}

interface InjectedProps {
  //
  commentsStateKeeper: CommentsStateKeeper;
}

@autobind
@observer
class CommentListActionButtonContainer extends ReactComponent<Props, {}, InjectedProps> {
  //
  static defaultProps = {
    onClick: () => {},
  };

  static contextType = CommentListContext;

  context!: ContextType<typeof CommentListContext>;

  onClick(event: React.MouseEvent) {
    //
    const { onClick } = this.propsWithDefault;
    const { commentList } = this.context;

    commentList.findMoreComments();
    onClick(event);
  }

  render() {
    const { commentsStateKeeper } = this.injected;
    const { offset } = commentsStateKeeper;
    const hasMoreComments = offset.totalCount > (offset.offset + offset.limit);

    return hasMoreComments && (
      <Box my={4} textAlign="center">
        <Button variant="text" onClick={this.onClick}>Show more Comments...</Button>
      </Box>
    );
  }
}

export default ServiceInjector.useContext(
  CommentsStateKeeper
)(CommentListActionButtonContainer);
