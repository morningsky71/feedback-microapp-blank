
import { CommentStateKeeper, CommentsStateKeeper } from './keeper';


export const store = {
  comment: {
    commentStateKeeper: CommentStateKeeper.instance,
    commentsStateKeeper: CommentsStateKeeper.instance,
  },
};

export {
  CommentStateKeeper,
  CommentsStateKeeper,
};
