import { runInAction } from 'mobx';
import {
  CommandResponse, makeExtendedObservable,
  mobxService,
  NameValueList,
  NotInstantiatedException,
  Operator,
  QueryParam,
} from '@nara.drama/prologue';
import {
  Comment,
  CommentApiStub,
  CommentCdo,
  CommentDynamicQuery,
  CommentFlowApiStub,
  CommentQuery,
  CommentQueryApiStub,
} from '../../../api';


@mobxService
class CommentStateKeeper {
  //
  static readonly instanceName = 'commentStateKeeper';
  static readonly serviceName = 'feedback.comment.commentStateKeeper';
  static instance: CommentStateKeeper;

  private readonly commentApi: CommentApiStub;
  private readonly commentFlowApi: CommentFlowApiStub;
  private readonly commentQueryApi: CommentQueryApiStub;

  comment: Comment | null = null;

  constructor(
    commentApi: CommentApiStub = CommentApiStub.instance,
    commentFlowApi: CommentFlowApiStub = CommentFlowApiStub.instance,
    commentQueryApi: CommentQueryApiStub = CommentQueryApiStub.instance
  ) {
    this.commentApi = commentApi;
    this.commentFlowApi = commentFlowApi;
    this.commentQueryApi = commentQueryApi;

    makeExtendedObservable(this);
  }

  async save(comment: Comment): Promise<CommandResponse> {
    //
    const isNew = !comment.id;
    let response;

    debugger;
    if (isNew) {
      response = await this.register(CommentCdo.fromModel(comment));
    }
    else {
      response = await this.modify(comment.id, Comment.asNameValues(comment));
    }

    return response;
  }

  async register(commentCdo: CommentCdo): Promise<CommandResponse> {
    return this.commentFlowApi.registerComment(commentCdo);
  }

  async modify(commentId: string, nameValues: NameValueList): Promise<CommandResponse> {
    return this.commentApi.modifyComment(commentId, nameValues);
  }

  async remove(commentId: string): Promise<CommandResponse> {
    return this.commentFlowApi.removeComment(commentId);
  }

  async findCommentById(commentId: string): Promise<Comment> {
    const commentQuery = CommentQuery.byId(commentId);
    const comment = await this.commentQueryApi.executeCommentQuery(commentQuery);

    runInAction(() => this.comment = comment);
    return comment;
  }

  async findCommentByFeedbackIdAndWriterId(feedbackId: string, writerId: string): Promise<Comment | null> {
    //
    const query = CommentDynamicQuery.multiParams<Comment>(
      QueryParam.andParam('feedbackId', Operator.Equal, feedbackId),
      QueryParam.endParam('writerId', Operator.Equal, writerId)
    );

    const comment = await this.commentQueryApi.executeCommentDynamicQuery(query)
      .catch(() => null);

    runInAction(() => this.comment = comment);

    return comment;
  }

  setCommentProp(name: keyof Comment, value: any) {
    if (!this.comment) {
      throw new NotInstantiatedException('CommentAgent.setCommentProp', 'comment is null');
    }

    (this.comment as any)[name] = value;
  }

  initComment(writerId: string, writerName: string, feedbackId: string) {
    //
    this.comment = new Comment(writerId, writerName, '', '', feedbackId, false, false, 0);
  }

  clear() {
    this.comment = null;
  }
}


CommentStateKeeper.instance = new CommentStateKeeper();

export default CommentStateKeeper;
