import { runInAction } from 'mobx';
import {
  makeExtendedObservable,
  mobxService,
  Offset,
  OffsetElementList,
  Operator,
  QueryParam,
} from '@nara.drama/prologue';
import { Comment, CommentApiStub, CommentQueryApiStub, CommentsDynamicQuery } from '../../../api';


@mobxService
class CommentsStateKeeper {
  //
  static readonly instanceName = 'commentsStateKeeper';
  static readonly serviceName = 'feedback.comment.commentsStateKeeper';
  static instance: CommentsStateKeeper;

  private readonly commentApi: CommentApiStub;
  private readonly commentQueryApi: CommentQueryApiStub;

  comments: Comment[] = [];

  offset: Offset = {} as Offset;


  constructor(
    commentApi: CommentApiStub = CommentApiStub.instance,
    commentQueryApi: CommentQueryApiStub = CommentQueryApiStub.instance
  ) {
    this.commentApi = commentApi;
    this.commentQueryApi = commentQueryApi;

    makeExtendedObservable(this);
  }

  async findCommentsByFeedbackId(feedbackId: string, offset: Offset): Promise<OffsetElementList<Comment>> {
    //
    const query = CommentsDynamicQuery.oneParam<Comment>(
      QueryParam.endParam('feedbackId', Operator.Equal, feedbackId)
    );

    query.offset = offset;

    return this.findComments(query);
  }

  async findNotDeletedCommentsByFeedbackId(feedbackId: string, offset: Offset): Promise<OffsetElementList<Comment>> {
    //
    const query = CommentsDynamicQuery.multiParams<Comment>(
      QueryParam.andParam('feedbackId', Operator.Equal, feedbackId),
      QueryParam.endParam('deleted', Operator.Equal, 'false')
    );

    query.offset = offset;

    return this.findComments(query);
  }

  private async findComments(query: CommentsDynamicQuery): Promise<OffsetElementList<Comment>> {
    //
    const commentsOffsetElementList = await this.commentQueryApi.executeCommentsDynamicPagingQuery(query);

    query.offset.totalCount = commentsOffsetElementList.totalCount;

    runInAction(() => {
      this.comments = commentsOffsetElementList.results;
      this.offset = query.offset;
    });

    return commentsOffsetElementList;
  }

  setCommentsProp(commentId: string, name: keyof Comment, value: any) {

    //
    // TODO: commentId로 코멘트 단건의 property 수정
  
     const findcomment = this.comments.find( (comment) => {  comment.writerId === commentId;} )

     //존재할 경우 수정
     if(findcomment){

        findcomment.writerId   = commentId;
        findcomment.writerName = value;

     }else{
      //존재하지 않을 경우 추가
      const findcomment = new Comment(commentId,name,"","","",false,false,122334);

      findcomment.writerId = commentId;
      findcomment.writerName = name;

      this.comments.push(findcomment);

     }

  }

  clear() {
    //
    this.comments = [];
  }
}


CommentsStateKeeper.instance = new CommentsStateKeeper();

export default CommentsStateKeeper;
