
import { store as commentStore } from './comment';

export default {
  ...commentStore,
};
