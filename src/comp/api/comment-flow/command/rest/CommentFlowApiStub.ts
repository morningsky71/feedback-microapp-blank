import { ApiClient, CommandResponse } from '@nara.drama/prologue';
import {
  RegisterCommentCommand,
  RemoveCommentCommand,
} from '../command';
import {
  CommentCdo,
} from '../../../comment'


class CommentFlowApiStub {
  static instance: CommentFlowApiStub;
  private readonly client = new ApiClient('/api/feedback/secure/comment-flow', { resDataName: 'commandResponse' });

  async registerComment(commentCdo: CommentCdo): Promise<CommandResponse> {
    const command = RegisterCommentCommand.new(
      commentCdo,
    );
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/register-comment', command);
  }

  async removeComment(commentId: string): Promise<CommandResponse> {
    const command = RemoveCommentCommand.new(
      commentId,
    );
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/remove-comment', command);
  }
}

CommentFlowApiStub.instance = new CommentFlowApiStub();

export default CommentFlowApiStub;
