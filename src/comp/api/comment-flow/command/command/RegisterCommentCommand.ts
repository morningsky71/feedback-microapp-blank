import { CqrsUserCommand } from '@nara.drama/prologue';
import { CommentCdo } from '../../../comment';


class RegisterCommentCommand extends CqrsUserCommand {
  commentCdo: CommentCdo;

  constructor(commentCdo: CommentCdo) {
    super();
    this.commentCdo = commentCdo;
  }

  static new(commentCdo: CommentCdo) {
    const command = new RegisterCommentCommand(
      commentCdo,
    );
    return command;
  }

}

export default RegisterCommentCommand;
