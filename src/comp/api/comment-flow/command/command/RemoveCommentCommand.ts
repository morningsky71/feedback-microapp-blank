import { CqrsUserCommand } from '@nara.drama/prologue';


class RemoveCommentCommand extends CqrsUserCommand {
  commentId: string;

  constructor(commentId: string) {
    super();
    this.commentId = commentId;
  }

  static new(commentId: string) {
    const command = new RemoveCommentCommand(
      commentId,
    );
    return command;
  }

}

export default RemoveCommentCommand;
