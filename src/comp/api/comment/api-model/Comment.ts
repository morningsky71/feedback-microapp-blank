import {
  NameValueList,
  fromDomain,
  DomainEntity,
  makeExtendedObservable,
} from '@nara.drama/prologue';
import moment from 'moment';

@fromDomain
class Comment extends DomainEntity {
  writerId: string;
  writerName: string;
  message: string;
  base64AttachedImage: string;
  feedbackId: string;
  important: boolean;
  deleted: boolean;
  time: number;

  /* for ui */
  expanded: boolean = false;
  editing: boolean = false;

  constructor(
    writerId: string,
    writerName: string,
    message: string,
    base64AttachedImage: string,
    feedbackId: string,
    important: boolean,
    deleted: boolean,
    time: number,
  ) {
    super();
    this.writerId = writerId;
    this.writerName = writerName;
    this.message = message;
    this.base64AttachedImage = base64AttachedImage;
    this.feedbackId = feedbackId;
    this.important = important;
    this.deleted = deleted;
    this.time = time;

    makeExtendedObservable(this);
  }

  static fromDomain(domain: Comment): Comment {
    const comment = new Comment(
      domain.writerId,
      domain.writerName,
      domain.message,
      domain.base64AttachedImage,
      domain.feedbackId,
      domain.important,
      domain.deleted,
      domain.time,
    );

    comment.setDomainEntity(domain);
    return comment;
  }

  static fromDomains(domains: Comment[]): Comment[] {
    return domains.map(domain => this.fromDomain(domain));
  }

  static asNameValues(comment: Comment) {
    //
    return NameValueList.fromModel(Comment, comment, {
      message: String,
      base64AttachedImage: String,
      important: String,
      // embeddedSubComments: JSON,
    });
  }

  get displayTime() {
    //
    return moment(this.time).format('YYYY-MM-DD');
  }
}

export default Comment;
