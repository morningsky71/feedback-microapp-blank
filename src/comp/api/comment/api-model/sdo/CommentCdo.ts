import Comment from '../Comment';


class CommentCdo {
  writerId: string;
  writerName: string;
  message: string;
  base64AttachedImage: string;
  feedbackId: string;

  constructor(writerId: string, writerName: string, message: string, base64AttachedImage: string, feedbackId: string) {
    this.writerId = writerId;
    this.writerName = writerName;
    this.message = message;
    this.base64AttachedImage = base64AttachedImage;
    this.feedbackId = feedbackId;
  }

  static fromModel(domain: Comment): CommentCdo {
    return new CommentCdo(
      domain.writerId,
      domain.writerName,
      domain.message,
      domain.base64AttachedImage,
      domain.feedbackId,
    );
  }

}

export default CommentCdo;
