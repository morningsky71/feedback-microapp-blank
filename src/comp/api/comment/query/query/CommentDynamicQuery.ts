import { fromDomain, CqrsDynamicQuery } from '@nara.drama/prologue';
import { Comment } from '../../api-model';


@fromDomain
class CommentDynamicQuery extends CqrsDynamicQuery<Comment> {
  static fromDomain(domain: CommentDynamicQuery): CommentDynamicQuery {
    const query = new CommentDynamicQuery();

    query.setResponse(domain);
    return query;
  }

}

export default CommentDynamicQuery;
