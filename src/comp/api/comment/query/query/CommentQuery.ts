import { fromDomain, CqrsBaseQuery } from '@nara.drama/prologue';
import { Comment } from '../../api-model';


@fromDomain
class CommentQuery extends CqrsBaseQuery<Comment> {
  commentId: string;

  constructor(commentId: string) {
    super(Comment);

    this.commentId = commentId;
  }

  static fromDomain(domain: CommentQuery): CommentQuery {
    const query = new CommentQuery(domain.commentId);

    query.setResponse(domain);
    return query;
  }

  static byId(id: string) {
    return new CommentQuery(id);
  }

}

export default CommentQuery;
