import { fromDomain, CqrsDynamicQuery } from '@nara.drama/prologue';
import { Comment } from '../../api-model';


@fromDomain
class CommentsDynamicQuery extends CqrsDynamicQuery<Comment> {
  static fromDomain(domain: CommentsDynamicQuery): CommentsDynamicQuery {
    const query = new CommentsDynamicQuery();

    query.setResponse(domain);
    return query;
  }

}

export default CommentsDynamicQuery;
