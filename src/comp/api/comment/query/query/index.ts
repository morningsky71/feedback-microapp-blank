export { default as CommentQuery } from './CommentQuery';
export { default as CommentsDynamicQuery } from './CommentsDynamicQuery';
export { default as CommentDynamicQuery } from './CommentDynamicQuery';
