import { ApiClient, OffsetElementList } from '@nara.drama/prologue';
import { Comment } from '../../api-model';
import { CommentQuery, CommentDynamicQuery, CommentsDynamicQuery } from '../../query';



class CommentQueryApiStub {
  static instance: CommentQueryApiStub;
  private readonly client = new ApiClient('/api/feedback/secure/comment/query', { resDataName: 'queryResult' });

  async executeCommentQuery(query: CommentQuery): Promise<Comment> {
    return this.client.postNotNull<Comment>(
      Comment,
      '/',
      query
    );
  }

  async executeCommentDynamicQuery(query: CommentDynamicQuery): Promise<Comment | null> {
    return this.client.postNullable<Comment>(
      Comment,
      '/dynamic-single',
      query
    );
  }

  async executeCommentsDynamicQuery(query: CommentsDynamicQuery): Promise<Comment[]> {
    return this.client.postArray<Comment>(
      Comment,
      '/dynamic-multi',
      query
    );
  }

  async executeCommentsDynamicPagingQuery(query: CommentsDynamicQuery): Promise<OffsetElementList<Comment>> {
    //
    return this.client.postOffsetElementList<Comment>(
      OffsetElementList,
      '/dynamic-multi',
      query,
    );
  }
}

CommentQueryApiStub.instance = new CommentQueryApiStub();

export default CommentQueryApiStub;
