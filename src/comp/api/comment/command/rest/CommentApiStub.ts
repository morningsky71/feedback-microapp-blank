import { ApiClient, CommandResponse, NameValueList } from '@nara.drama/prologue';
import {
  CommentCommand,
} from '../command';
import { CommentCdo } from '../../api-model';


class CommentApiStub {
  static instance: CommentApiStub;
  private readonly client = new ApiClient('/api/feedback/secure/comment', { resDataName: 'commandResponse' });

  async registerComment(commentCdo: CommentCdo): Promise<CommandResponse> {
    const command = CommentCommand.newRegisterCommentCommand(commentCdo);
    return this.executeComment(command);
  }

  async registerComments(commentCdos: CommentCdo[]): Promise<CommandResponse> {
    const command = CommentCommand.newRegisterCommentCommands(commentCdos);
    return this.executeComment(command);
  }

  async modifyComment(commentId: string, nameValues: NameValueList): Promise<CommandResponse> {
    const command = CommentCommand.newModifyCommentCommand(commentId, nameValues);
    return this.executeComment(command);
  }

  async removeComment(commentId: string): Promise<CommandResponse> {
    const command = CommentCommand.newRemoveCommentCommand(commentId);
    return this.executeComment(command);
  }

  async executeComment(commentCommand: CommentCommand): Promise<CommandResponse> {
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/comment/command', commentCommand);
  }

}

CommentApiStub.instance = new CommentApiStub();

export default CommentApiStub;
