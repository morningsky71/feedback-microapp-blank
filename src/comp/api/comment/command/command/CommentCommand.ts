import { CqrsBaseCommandType, CqrsBaseCommand, NameValueList } from '@nara.drama/prologue';
import { CommentCdo } from '../../api-model';


class CommentCommand extends CqrsBaseCommand {
  commentCdo: CommentCdo | null = null;
  commentCdos: CommentCdo[] = [];
  commentId: string | null = null;
  nameValues: NameValueList | null = null;

  static newRegisterCommentCommand(commentCdo: CommentCdo): CommentCommand {
    const command = new CommentCommand(CqrsBaseCommandType.Register);

    command.commentCdo = commentCdo;
    return command;
  }

  static newRegisterCommentCommands(commentCdos: CommentCdo[]): CommentCommand {
    const command = new CommentCommand(CqrsBaseCommandType.Register);

    command.commentCdos = commentCdos;
    return command;
  }

  static newModifyCommentCommand(commentId: string, nameValues: NameValueList): CommentCommand {
    const command = new CommentCommand(CqrsBaseCommandType.Modify);

    command.commentId = commentId;
    command.nameValues = nameValues;
    return command;
  }

  static newRemoveCommentCommand(commentId: string): CommentCommand {
    const command = new CommentCommand(CqrsBaseCommandType.Remove);

    command.commentId = commentId;
    return command;
  }

}

export default CommentCommand;
