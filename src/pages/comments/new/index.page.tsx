import React from 'react';
import { observer } from 'mobx-react';
import { NextRouter, withRouter } from 'next/router';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { CommentForm } from '../../../comp/view';

interface Props {
  router: NextRouter;
}

interface InjectedProps {
}
@observer
@autobind
class CommentRegistrationPage extends ReactComponent<Props, {}, InjectedProps> {
  //
  routeToList() {
    //
    // TODO: 코멘트 리스트 페이지로 이동
    this.props.router.push('/view/comment');
  }

  render() {
    //
    const sourceEntityId = "testFeedbackId";
    const sourceEntityName = 'Namoosori';
    const userId = 'manager@nextree.io';
    const writerName = 'testName';

    return (
      <CommentForm
        sourceEntityId={sourceEntityId}
        sourceEntityName={sourceEntityName}
        userId={userId}
        writerName={writerName}
        onSuccess={this.routeToList}
      />
    );
  }
}

export default withRouter(CommentRegistrationPage);
