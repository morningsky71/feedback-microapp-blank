import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { NextRouter, withRouter } from 'next/router';
import { CommentList } from '~/comp/view';
import { Button, Container, SubActions } from '@nara.platform/react-ui';


interface Props {
  router: NextRouter;
}

@autobind
class CommentListPage extends ReactComponent<Props> {
  //

  moveToCommentsNew(){
    this.props.router.push('/comments/new');

  }
  render() {
    //
    const sourceEntityId = "testFeedbackId";
    const sourceEntityName = 'Namoosori';
    const userId = 'manager@nextree.io';
    const writerName = 'testName';

    return (
      <Container>
        <CommentList
          sourceEntityId={sourceEntityId}
          sourceEntityName={sourceEntityName}
          userId={userId}
          writerName={writerName}
        >
          <SubActions>
            <SubActions.Left>
              <Button onClick={this.moveToCommentsNew}>등록버튼</Button>
              {/* TODO: 코멘트 등록 페이지(/comments/new)로 이동하는 버튼 구현 */}
            </SubActions.Left>
          </SubActions>

          <CommentList.Header />
          <CommentList.Content />
          <CommentList.ActionButton />
        </CommentList>
      </Container>
      );
  }
}

export default withRouter(CommentListPage);
