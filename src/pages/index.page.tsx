import React from 'react';
import { NextRouter, withRouter } from 'next/router';
import { autobind, injectFromName, ReactComponent } from '@nara.drama/prologue';

interface Props {
  router: NextRouter;
}

interface InjectedProps {
  //
}

@autobind
class IndexPage extends ReactComponent<Props, {}, InjectedProps> {
  //
  componentDidMount() {
    //
    // TODO: 코멘트 리스트 페이지로 이동

     //
    this.props.router.push('/comments');

  }

  render() {
    //
    return (
      <div>
        Hello, World!
      </div>
    );
  }
}

export default withRouter(IndexPage);
